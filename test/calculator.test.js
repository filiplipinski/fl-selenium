const fs = require("fs");
const { Builder, By } = require("selenium-webdriver");

const siteUrl = "http://www.anaesthetist.com/mnm/javascript/calc.htm";

describe("calculator selenium tests", () => {
  let driver;

  beforeAll(async () => {
    driver = new Builder().forBrowser("chrome").build();
    await driver.get(siteUrl);
  }, 10000);

  afterAll(async () => {
    await driver.quit();
  }, 15000);

  test("should calculate multiply correctly", async () => {
    await driver.findElement(By.name("three")).click();
    await driver.findElement(By.name("mul")).click();
    await driver.findElement(By.name("two")).click();
    await driver.findElement(By.name("result")).click();

    const value = await driver
      .findElement(By.name("Display"))
      .getAttribute("value");

    expect(value).toEqual("6");
  });

  test("should make a screeshot and save in project root", async () => {
    const image = await driver.takeScreenshot();
    await fs.promises.writeFile("site-screenshot.png", image, "base64");
  });
});
