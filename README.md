# Selenium

Autor kodu: Filip Lipiński

## Instalacja

Przykładowy kod został napisany w JavaScript-cie, do uruchomienia wymaga zainstalowanego `node.js`.
Niezbędne biblioteki instalujemy przy pomocy `npm install`.

Do działania kod wymaga jeszcze `chromedriver`, którego można pobrać z https://chromedriver.chromium.org/ (uwaga! należy pobrać wersje dla swojego Chrome-a). Program musi być dostępny w zmiennej środowiskowej `PATH`.

## Uruchomienie

Testy można uruchomić za pomocą komendy `npm test`.
